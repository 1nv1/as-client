#ifndef LUASCRIPT_H
#define LUASCRIPT_H

#include <string>
#include <vector>
#include <iostream>

// Lua is written in C, so compiler needs to know how to link its libraries
#include <luajit-2.0/lua.hpp>

class LuaScript {
public:
    LuaScript(const std::string& filename);
    ~LuaScript();
    void printError(const std::string& variableName, const std::string& reason);

    template<typename T>
    T get(const std::string& variableName) {
      if(!L) {
        printError(variableName, "Script is not loaded");
        return lua_getdefault<T>();
      }

      T result;
      if(lua_gettostack(variableName)) { // variable succesfully on top of stack
        result = lua_get<T>(variableName);
      } else {
        result = lua_getdefault<T>();
      }

      lua_pop(L, level + 1); // pop all existing elements from stack
      return result;
    }

    bool lua_gettostack(const std::string& variableName) {
    level = 0;
    std::string var = "";
    for(unsigned int i = 0; i < variableName.size(); i++) {
      if(variableName.at(i) == '.') {
        if(level == 0) {
          lua_getglobal(L, var.c_str());
        } else {
          lua_getfield(L, -1, var.c_str());
        }

        if(lua_isnil(L, -1)) {
          printError(variableName, var + " is not defined");
          return false;
        } else {
          var = "";
          level++;
        }
      } else {
        var += variableName.at(i);
      }
    }
    if(level == 0) {
      lua_getglobal(L, var.c_str());
    } else {
      lua_getfield(L, -1, var.c_str());
    }
    if(lua_isnil(L, -1)) {
        printError(variableName, var + " is not defined");
        return false;
    }

    return true;
    }

    // Generic get
    template<typename T>
    T lua_get(const std::string& variableName) {
      return 0;
    }

    // Generic default get
    template<typename T>
    T lua_getdefault() {
      return 0;
    }

    lua_State const *lua_state() const { return L; }
    lua_State *lua_state() { return L; }

    template<typename T>
    std::vector<T> getArray(const std::string& name)
    {
        return std::vector<T>();
    }

    void clean() {
        int n = lua_gettop(L);
        lua_pop(L, n);
    }

public:
    lua_State* L;
    int level;
};

LuaScript::LuaScript(const std::string& filename) {
    L = luaL_newstate();
    if (luaL_loadfile(L, filename.c_str()) || lua_pcall(L, 0, 0, 0)) {
        printf("Error: script not loaded (%s)\n", filename.data());
        L = 0;
    }
}

LuaScript::~LuaScript() {
    if(L) lua_close(L);
}

void LuaScript::printError(const std::string& variableName, const std::string& reason) {
    std::cout<<"Error: can't get ["<<variableName<<"]. "<<reason<<std::endl;
}

template<>
inline std::string LuaScript::lua_getdefault() {
  return "null";
}

template <>
inline bool LuaScript::lua_get(const std::string& variableName) {
    return (bool)lua_toboolean(L, -1);
}

template <>
inline float LuaScript::lua_get(const std::string& variableName) {
    if(!lua_isnumber(L, -1)) {
      printError(variableName, "Not a number");
    }
    return (float)lua_tonumber(L, -1);
}

template <>
inline int LuaScript::lua_get(const std::string& variableName) {
    if(!lua_isnumber(L, -1)) {
      printError(variableName, "Not a number");
    }
    return (int)lua_tonumber(L, -1);
}

template <>
inline std::string LuaScript::lua_get(const std::string& variableName) {
    std::string s = "null";
    if(lua_isstring(L, -1)) {
      s = std::string(lua_tostring(L, -1));
    } else {
      printError(variableName, "Not a string");
    }
    return s;
}

template <>
std::vector<int> LuaScript::getArray(const std::string& name) {
    std::vector<int> v;
    lua_getglobal(L, name.c_str());
    if(lua_isnil(L, -1)) {
        return std::vector<int>();
    }
    lua_pushnil(L);
    while(lua_next(L, -2)) {
        v.push_back((int)lua_tonumber(L, -1));
        lua_pop(L, 1);
    }
    clean();
    return v;
}

template <>
std::vector<std::string> LuaScript::getArray(const std::string& name) {
    std::vector<std::string> v;
    lua_getglobal(L, name.c_str());
    if(lua_isnil(L, -1)) {
        return std::vector<std::string>();
    }
    lua_pushnil(L);
    while(lua_next(L, -2)) {
        v.push_back((std::string)lua_tostring(L, -1));
        lua_pop(L, 1);
    }
    clean();
    return v;
}

void stack_trace(lua_State *L)
{
    int i;
    int top = lua_gettop(L);
    printf("---- Begin Stack ----\n");
    printf("Stack size: %i\n\n", top);
    for (i = top; i >= 1; i--)
    {
        int t = lua_type(L, i);
        switch (t)
        {
            case LUA_TSTRING:
            {
                printf("%i -- (%i) ---- `%s'", i, i - (top + 1), lua_tostring(L, i));
                break;
            }

            case LUA_TBOOLEAN:
            {
                printf("%i -- (%i) ---- %s", i, i - (top + 1), lua_toboolean(L, i) ? "true" : "false");
                break;
            }

            case LUA_TNUMBER:
            {
                printf("%i -- (%i) ---- %g", i, i - (top + 1), lua_tonumber(L, i));
                break;
            }

            default:
            {
                printf("%i -- (%i) ---- %s", i, i - (top + 1), lua_typename(L, t));
                break;
            }
        }
        printf("\n");
    }
    printf("---- End Stack ----\n");
    printf("\n");
}

#endif
