cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

set(Boost_USE_MULTITHREADED ON) 
set(Boost_USE_STATIC_RUNTIME OFF) 

set(BOOST_VER "1.55" CACHE STRING "boost version" )
find_package(Boost ${BOOST_VER} REQUIRED COMPONENTS system date_time random) 
find_package(OpenSSL)
find_package(ALSA)

link_directories(
    ${CMAKE_FIND_ROOT_PATH}/lib
    ${CMAKE_FIND_ROOT_PATH}/usr/local/lib
    ${ALSA_LIBRARY}
    ${LUAJIT_ROOT}/lib
)

add_executable(as_client main.cpp)
set_property(TARGET as_client PROPERTY CXX_STANDARD 11)
set_property(TARGET as_client PROPERTY CXX_STANDARD_REQUIRED ON)

target_link_libraries(as_client dl rt sioclient pthread)
target_link_libraries(as_client luajit-5.1 asound ${Boost_LIBRARIES} ${OPENSSL_LIBRARIES})

target_include_directories(as_client PRIVATE ${Boost_INCLUDE_DIRS}
    ${CMAKE_FIND_ROOT_PATH}/include
    ${CMAKE_FIND_ROOT_PATH}/usr/local/include 
    ${ALSA_INCLUDE_DIR}
    ${LUAJIT_ROOT}/include
)

set(CMAKE_CXX_FLAGS "-Wall -Wextra -O3")

# message(STATUS ${DEPENDS_DIR} )
#target_include_directories(as_client PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../src" ${Boost_INCLUDE_DIRS} )

