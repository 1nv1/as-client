Capture audio then streaming to server

Use with ```audiostreaming```

Dependencies:

- boost 1.65
- openSSL 1.1g
- socket.io-client-cpp 1.6.1
- websocketpp 0.7.0
- alsa-lib-1.1.3
- LuaJIT-2.0.5
- cmake 3.10
 
Compile:

- clone all submodules: git submodule update --init --recursive
- checkout to appropriate version
- run: ```mkdir build && cd build && cmake .. && make```