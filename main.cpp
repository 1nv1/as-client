#include <iostream>
#include <vector>
#include <string>

#include <cstdio>
#include <cstring>
#include <error.h>

#include <unistd.h>
#include <fstream>
#include <alsa/asoundlib.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <sio_client.h>

#include <chrono>
#include <thread>

#include <luajit-2.0/lua.hpp>

#include "LuaScript.h"

#define _LOGI(format, ...) do {printf("[I-%d] %s %s [%d] " format "\n", (int)getpid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ );} while(0)

#define _LOGE(format, ...) do {fprintf(stderr, "[E-%d] %s %s [%d] " format "%s\n", (int)getpid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)

struct AlsaConfig {
    std::string mHw;
    uint32_t mSampleRate;
    int mChannel;
    snd_pcm_format_t mFormat;
    uint32_t mFragment;
    snd_pcm_uframes_t mFrames;
};

// std::string const HW_NAME = "plughw:1,0";
// uint32_t const SAMPLE_RATE = 44100;
// int const CHANNELS = 1;
// snd_pcm_format_t const PCM_FORMAT = SND_PCM_FORMAT_FLOAT_LE;

// uint32_t const FRAGMENTS = 4;
// uint32_t const FRAMES = 512;

uint32_t const MS_PER_SEC = 1000;

std::mutex _lock;

std::condition_variable _cond;

bool gReady=false;

#define SAFE_DEL(obj) if(obj) {delete obj; obj = nullptr;}

class AlsaRecord
{
public:
    AlsaRecord() = default;

    virtual ~AlsaRecord()
    {
        if(mHandle) snd_pcm_close(mHandle);
        snd_config_update_free_global();
    }

    AlsaRecord(AlsaConfig aConfig)
        : mHandle(nullptr) 
        , mConfig(aConfig)
        {}

    int init()
    {
        int err, dir;

        /* Open PCM device for recording (capture) */
        err = snd_pcm_open(&mHandle, mConfig.mHw.data(), SND_PCM_STREAM_CAPTURE, 0);
        _LOGI("%p", mHandle);
        if (err)
        {
            _LOGE("ERROR setting interleaved mode: %s", snd_strerror(err));
            return 1;
        }


        /* Allocate a hardware parameters object. */
        snd_pcm_hw_params_alloca(&mParams);

        /* Fill it in with default values. */
        snd_pcm_hw_params_any(mHandle, mParams);

        err = snd_pcm_hw_params_set_access(mHandle, mParams, SND_PCM_ACCESS_RW_INTERLEAVED);
        if (err)
        {
            _LOGE("ERROR setting interleaved mode: %s", snd_strerror(err));
            return 1;
        }

        /* Signed 16-bit little-endian format */
        err = snd_pcm_hw_params_set_format(mHandle, mParams, mConfig.mFormat); //EDIT

        if (err)
        {
            _LOGE("ERROR setting format: %s", snd_strerror(err));
            return 1;
        }
        /* Set number of channels */
        err = snd_pcm_hw_params_set_channels(mHandle, mParams, mConfig.mChannel); //EDIT

        if (err)
        {
            _LOGE("ERROR setting channels: %s", snd_strerror(err));
            return 1;
        }
        /* Set sample rate */
        err = snd_pcm_hw_params_set_rate_near(mHandle, mParams, &mConfig.mSampleRate, 0);

        if (err)
        {
            _LOGE("ERROR setting sampling rate (%d): %s", mConfig.mSampleRate, snd_strerror(err));
            return 1;
        }

        /* Set period size*/
        err = snd_pcm_hw_params_set_period_size_near(mHandle, mParams, &mConfig.mFrames, &dir);
        if (err)
        {
            _LOGE("ERROR setting period size: %s", snd_strerror(err));
            return 1;
        }
        /* Write the parameters to the driver */
        err = snd_pcm_hw_params(mHandle, mParams);

        if (err < 0)
        {
            _LOGE("UNABLE to set HW parameters: %s", snd_strerror(err));
            return 1;
        }
        /* Use a buffer large enough to hold one period */
        err = snd_pcm_hw_params_get_period_size(mParams, &mConfig.mFrames, &dir);

        if (err)
        {
            _LOGE("ERROR retrieving period size: %s", snd_strerror(err));
            return 1;
        }
        int sampleSize = snd_pcm_format_physical_width(mConfig.mFormat) / 8;
        int size = mConfig.mFrames * sampleSize * mConfig.mChannel * mConfig.mFragment;
        // mBuffer = new uint8_t[size];
        mBuffer.reserve(size);
        mpBuffer = &mBuffer[0];

        err = snd_pcm_hw_params_get_period_time(mParams, &mPeriodTime, &dir);

        if (err)
        {
            _LOGE("Error retrieving period time: %s", snd_strerror(err));
            return 1;
        }

        return 0;
    }

    int capture()
    {
        int lRet;
        // while(true)
        // {
            lRet = snd_pcm_readi(mHandle, mpBuffer, mConfig.mFrames);

            if (lRet == -EPIPE)
            {
                /* EPIPE means overrun */
                _LOGE("OVERRUN OCCURRED");
                snd_pcm_prepare(mHandle);
            }
            else if (lRet < 0)
            {
                _LOGE("ERROR FROM READ: %s", snd_strerror(lRet));
                return 1;
            }
            else if (lRet != (int)mConfig.mFrames)
            {
                // _LOGE("SHORT READ, read %d frames", lRet);
            }
        // }

        return lRet;
    }

    snd_pcm_t *mHandle;
    snd_pcm_hw_params_t *mParams;
    AlsaConfig mConfig;
    // snd_pcm_uframes_t mFrames;

    uint32_t mPeriodTime;
    char *mpBuffer;
    std::vector<char> mBuffer;
};

int main(int argc, char **argv)
{
    _LOGI("Reading config");

    // AlsaConfig config = {
    //     .mHw = "plughw:1,0",
    //     .mSampleRate = 44100,
    //     .mChannel = 1,
    //     .mFormat = SND_PCM_FORMAT_FLOAT_LE,
    //     .mFragment = 4,
    //     .mFrames = 512,
    // };

    LuaScript lua(argv[1]);

    _LOGI("Init ALSA");

    AlsaConfig config = {
        .mHw = lua.get<std::string>("AlsaConfig.Hw"),
        .mSampleRate = static_cast<uint32_t>(lua.get<int>("AlsaConfig.SampleRate")),
        .mChannel = lua.get<int>("AlsaConfig.Channel"),
        .mFormat = static_cast<snd_pcm_format_t>(lua.get<int>("AlsaConfig.Format")),
        // .mFormat = SND_PCM_FORMAT_FLOAT_LE,
        .mFragment = static_cast<uint32_t>(lua.get<int>("AlsaConfig.Fragment")),
        .mFrames = static_cast<uint32_t>(lua.get<int>("AlsaConfig.Frames")),
    };

    stack_trace(lua.L);

    _LOGI("%s", config.mHw.data());
    _LOGI("%d", config.mSampleRate);
    _LOGI("%d", config.mChannel);
    _LOGI("%d", config.mFormat);
    _LOGI("%d", config.mFragment);
    _LOGI("%d", config.mFrames);

    AlsaRecord a(config);
    int lRet = a.init();

    _LOGI("init: %d", lRet);
    _LOGI("mSampleRate: %d - mFrames: %d", a.mConfig.mSampleRate, a.mConfig.mFrames);

    _LOGI("mPeriodTime: %d", a.mPeriodTime);

    _LOGI("Init Network");

    std::string url=lua.get<std::string>("NetConfig.ServerUrl");
    int debug = lua.get<int>("NetConfig.debug");

    sio::client h;
    h.set_open_listener([&]()
    {
        _LOGI("");
        // h.close();
        gReady = true;
        _cond.notify_all();
    });

    h.connect(url);

    {
        std::unique_lock<std::mutex> lk(_lock);
        _cond.wait(lk, []{return gReady;});
    }

    _LOGI("");

    std::ofstream ofs;
    if(debug) ofs.open(argv[2], std::ios::out | std::ios::binary);

    while(true)
    {
        // using namespace std;
        // using namespace std::chrono;
        // high_resolution_clock::time_point lbeg = high_resolution_clock::now();

        int frames = a.capture();
        if(frames < a.mConfig.mFrames) continue;
        int lsize = a.mConfig.mFrames * (snd_pcm_format_physical_width(a.mConfig.mFormat) / 8);
        if(lsize > 0)
        {
            if(debug) _LOGI("Emit bcdata: frames: %d - size: %d", frames, lsize);
            h.socket()->emit("bcdata", std::make_shared<std::string>(a.mpBuffer, lsize));

            if(debug) ofs.write(a.mpBuffer, lsize);
        }

        // auto elapsed = duration_cast<microseconds>(high_resolution_clock::now() - lbeg);
        
        // cout << elapsed.count() << " : " << microseconds(a.mPeriodTime).count() << endl;
        // cout << (microseconds(a.mPeriodTime) - elapsed).count() << endl;
        // _LOGI("%f", microseconds(elapsed).count());
        // _LOGI("%f", microseconds(a.mPeriodTime).count());

        // if(elapsed < microseconds(a.mPeriodTime))
        // {
            // _LOGI("sleep: %d", (microseconds(a.mPeriodTime - elapsed.count())));
            // this_thread::sleep_for(microseconds(a.mPeriodTime - elapsed.count()));
        // }
        // else
        // {
            // _LOGI("elasped: %d", elapsed.count());
        // }
    }

    h.sync_close();
    h.clear_con_listeners();

    _LOGI("");

    return 0;
}
